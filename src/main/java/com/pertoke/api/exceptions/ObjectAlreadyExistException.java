package com.pertoke.api.exceptions;

/**
 * Created by lulzimgashi on 14/06/2017.
 */
public class ObjectAlreadyExistException extends Throwable {

    public ObjectAlreadyExistException(String message) {
        super(message);
    }
}