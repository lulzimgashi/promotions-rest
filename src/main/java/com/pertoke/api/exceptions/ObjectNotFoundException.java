package com.pertoke.api.exceptions;

/**
 * Created by lulzimgashi on 14/06/2017.
 */
public class ObjectNotFoundException extends Throwable {

    public ObjectNotFoundException(String message) {
        super(message);
    }
}