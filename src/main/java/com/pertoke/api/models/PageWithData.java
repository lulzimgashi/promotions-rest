package com.pertoke.api.models;

import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by lulzimgashi on 14/06/2017.
 */
@Service
public class PageWithData<T> {
    private int currentPage;
    private int allPages;
    private List<T> elements;


    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getAllPages() {
        return allPages;
    }

    public void setAllPages(int allPages) {
        this.allPages = allPages;
    }

    public List<T> getElements() {
        return elements;
    }

    public void setElements(List<T> elements) {
        this.elements = elements;
    }
}
