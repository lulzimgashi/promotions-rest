package com.pertoke.api.models;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by lulzimgashi on 13/06/2017.
 */
@Entity()
@Table(name = "images")
public class Image {

    @Id
    private String id;

    private String url;

    @Column(name = "product_id", columnDefinition = "varchar(36)")
    private String productId;

    @Column(name = "category_id", columnDefinition = "varchar(36)")
    private String categoryId;

    @Column(name = "company_id", columnDefinition = "varchar(36)")
    private String companyId;

    @Column(name = "is_icon")
    private boolean isIcon;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public boolean isIcon() {
        return isIcon;
    }

    public void setIcon(boolean icon) {
        isIcon = icon;
    }


}
