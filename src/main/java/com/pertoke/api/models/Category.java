package com.pertoke.api.models;

import org.hibernate.annotations.GenericGenerator;


import javax.persistence.*;

/**
 * Created by lulzimgashi on 13/06/2017.
 */
@Entity()
@Table(name = "categories")
public class Category {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(columnDefinition = "varchar(36)")
    private String id;

    private String name;

    @Transient
    private Image iconImage;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Image getIconImage() {
        return iconImage;
    }

    public void setIconImage(Image iconImage) {
        this.iconImage = iconImage;
    }


}
