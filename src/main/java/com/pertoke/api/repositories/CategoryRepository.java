package com.pertoke.api.repositories;

import com.pertoke.api.models.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by lulzimgashi on 13/06/2017.
 */
@Repository
public interface CategoryRepository extends JpaRepository<Category, String> {
}
