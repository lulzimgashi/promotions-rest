package com.pertoke.api.repositories;

import com.pertoke.api.models.Notification;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by lulzimgashi on 25/07/2017.
 */
public interface NotificationRepository extends JpaRepository<Notification, String> {
}
