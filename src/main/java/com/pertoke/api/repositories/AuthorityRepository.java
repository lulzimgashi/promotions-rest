package com.pertoke.api.repositories;

import com.pertoke.api.models.Authority;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by lulzimgashi on 14/06/2017.
 */
@Repository
public interface AuthorityRepository extends JpaRepository<Authority, String> {

    @Modifying
    @Transactional
    @Query("update Authority u SET u.username=?1,u.authority=?2 WHERE u.userId=?3 ")
    void updateAuthority(String username, String role, String id);

    Authority findByUsername(String username);


    void deleteByUserId(String id);

}