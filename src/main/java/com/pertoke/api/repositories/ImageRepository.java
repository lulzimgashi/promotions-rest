package com.pertoke.api.repositories;

import com.pertoke.api.models.Image;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by lulzimgashi on 13/06/2017.
 */
@Repository
public interface ImageRepository extends JpaRepository<Image, String> {

    List<Image> findAllByCategoryIdOrProductIdOrCompanyId(String CategoryId, String ProductId, String CompanyId);

    void deleteAllByProductIdOrCategoryIdOrCompanyId(String ProductId, String CategoryId, String CompanyId);

    @Query("select i from Image i where i.productId=?1 AND i.isIcon = TRUE")
    Image imageByProductId(String productId);
}
