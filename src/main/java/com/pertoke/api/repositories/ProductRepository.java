package com.pertoke.api.repositories;

import com.pertoke.api.models.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by lulzimgashi on 13/06/2017.
 */
@Repository
public interface ProductRepository extends JpaRepository<Product, String> {

}
