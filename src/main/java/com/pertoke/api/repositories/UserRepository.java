package com.pertoke.api.repositories;

import com.pertoke.api.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by lulzimgashi on 14/06/2017.
 */
@Repository
public interface UserRepository extends JpaRepository<User, String> {

    @Modifying
    @Transactional
    @Query("update User u set u.enabled=0 where u.id=?1")
    void dUser(String userId);

    @Query("select CASE WHEN COUNT(u) > 0 THEN true ELSE false END from User u where u.username=?1 AND u.id<>?2")
    boolean checkIfExist(String username, String id);

    User findByUsername(String username);

    User findByIdAndEnabledTrue(String id);


}