package com.pertoke.api.repositories;

import com.pertoke.api.models.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by lulzimgashi on 13/06/2017.
 */
@Repository
public interface CompanyRepository extends JpaRepository<Company, String> {

    List<Company> findAllByNameOrLocation(String name, String location);

    @Modifying
    @Transactional
    @Query("update Company u SET u.timestamp=CURRENT_TIMESTAMP WHERE u.id=?1")
    void addTopCompany(String id);
}