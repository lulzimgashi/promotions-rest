package com.pertoke.api.utilities;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by lulzimgashi on 25/07/2017.
 */
@Service
public class FCMHelper {

    @Value("${API_URL_FCM}")
    private String API_URL_FCM;

    @Value("${AUTH_KEY_FCM}")
    private String AUTH_KEY_FCM;

    @Value("${TOPIC}")
    private String TOPIC;


    public String sendNotification(String title, String body, String postId) throws Exception {

        String authKey = AUTH_KEY_FCM;   // You FCM AUTH key
        String FMCurl = API_URL_FCM;

        URL url = new URL(FMCurl);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        conn.setUseCaches(false);
        conn.setDoInput(true);
        conn.setDoOutput(true);

        conn.setRequestMethod("POST");
        conn.setRequestProperty("Authorization", "key=" + authKey);
        conn.setRequestProperty("Content-Type", "application/json");


        //Construct notification object
        JSONObject json = new JSONObject();
        json.put("to", "/topics/" + TOPIC);

        JSONObject info = new JSONObject();
        info.put("title", title);   // Notification title
        info.put("body", body); // Notification body
        json.put("notification", info);

        JSONObject data = new JSONObject();
        data.put("postId", postId);
        json.put("data", data);


        OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
        wr.write(json.toString());
        wr.flush();


        String response = readInputStreamToString(conn.getInputStream());
        if (response == null) {
            return response;
        }
        JSONParser parser = new JSONParser();
        Object parse = parser.parse(response);
        JSONObject jsonObject = (JSONObject) parse;
        Long message_id = (Long) jsonObject.get("message_id");
        if (message_id == null) {
            return null;
        } else {
            return message_id.toString();
        }
    }

    private String readInputStreamToString(InputStream inputStream) {
        String result = null;
        StringBuffer sb = new StringBuffer();
        InputStream is = null;

        try {
            is = new BufferedInputStream(inputStream);
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String inputLine = "";
            while ((inputLine = br.readLine()) != null) {
                sb.append(inputLine);
            }
            result = sb.toString();
        } catch (Exception e) {
            result = null;
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                }
            }
        }

        return result;
    }
}
