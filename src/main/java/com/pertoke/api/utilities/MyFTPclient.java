package com.pertoke.api.utilities;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by lulzimgashi on 07/06/2017.
 */
@Service
public class MyFTPclient {

    @Value("${ftp-host}")
    private String ftp_host;

    @Value("${ftp-user}")
    private String ftp_user;

    @Value("${ftp-password}")
    private String ftp_password;


    FTPClient ftpClient = new FTPClient();


    public void upload(InputStream multipartFile, String uuid) throws IOException {

        boolean uploaded = false;
        try {
            uploaded = ftpClient.storeFile("images/" + uuid + ".png", multipartFile);
            if (multipartFile.read() == -1) {
                multipartFile.close();
            }
        } catch (IOException e) {
            try {
                ftpClient.disconnect();
                init();
                if (!uploaded) {
                    upload(multipartFile, uuid);
                }
            } catch (IOException e2) {

            }
        }

    }

    public void delete(String name) throws IOException {
        name = name.replace(ftp_host, "");
        boolean deleted = false;
        try {
            deleted = ftpClient.deleteFile(name);
        } catch (IOException e) {
            try {
                ftpClient.disconnect();
                init();
                if (!deleted) {
                    ftpClient.deleteFile(name);
                }
            } catch (IOException e2) {

            }
        }
    }

    @PostConstruct
    public void init() throws IOException {

        ftpClient.connect(ftp_host, 21);
        ftpClient.login(ftp_user, ftp_password);
        ftpClient.enterLocalPassiveMode();

        ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
    }
}