package com.pertoke.api.db;

/**
 * Created by lulzimgashi on 15/06/2017.
 */
public class PRODUCT_SQL {
    public static final String GET_ALL_PRODUCTS_SQL = "SELECT p.*, co.*, ca.*, img.*, co_image.*, ca_image.* FROM products p INNER JOIN companies co ON p.company_id = co.id INNER JOIN categories ca ON ca.id = p.category_id LEFT JOIN images img ON p.id = img.product_id LEFT JOIN images co_image ON co.id = co_image.company_id LEFT JOIN images ca_image ON ca.id = ca_image.category_id WHERE IF(length(:companyName) > 0, co.name = :companyName, TRUE) AND IF(length(:categoryName) > 0, ca.name = :categoryName, TRUE) AND IF(length(:dateFrom) > 0 AND length(:dateTo) > 0,(p.date_from >= :dateFrom AND p.date_to <= :dateTo), TRUE) AND IF(length(:dateFrom) > 0, p.date_from >= :dateFrom, TRUE) AND IF(length(:dateTo) > 0, p.date_to <= :dateTo, TRUE) AND IF(length(:name) > 0, p.name LIKE CONCAT('%',:name,'%'), TRUE) AND IF(length(:price) > 0, FLOOR(p.new_price) = :price, TRUE) LIMIT :start, :end;";

    public static final String GET_ALL_PRODUCTS_FOR_COMPANY_SQL = "SELECT p.*, co.*, ca.*, img.*, co_image.*, ca_image.* FROM products p INNER JOIN companies co ON p.company_id = co.id INNER JOIN categories ca ON ca.id = p.category_id LEFT JOIN images img ON p.id = img.product_id LEFT JOIN images co_image ON co.id = co_image.company_id LEFT JOIN images ca_image ON ca.id = ca_image.category_id WHERE p.company_id=:companyId";

    public static final String GET_PRODUCT_SQL = "SELECT p.*, co.*, ca.*, img.*, co_image.*, ca_image.* FROM products p INNER JOIN companies co ON p.company_id = co.id INNER JOIN categories ca ON ca.id = p.category_id LEFT JOIN images img ON p.id = img.product_id LEFT JOIN images co_image ON co.id = co_image.company_id LEFT JOIN images ca_image ON ca.id = ca_image.category_id WHERE p.id = ?;";
}
