package com.pertoke.api.services;

import com.pertoke.api.db.PRODUCT_SQL;
import com.pertoke.api.models.*;
import com.pertoke.api.repositories.CompanyRepository;
import com.pertoke.api.repositories.ImageRepository;
import com.pertoke.api.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by lulzimgashi on 13/06/2017.
 */
@SuppressWarnings("ALL")
@Service
public class CompanyService {

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private ImageRepository imageRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Transactional(readOnly = true)
    public List<Company> getAllCompaniesByFilters(String name, String location) {

        List<Company> allByNameOrLocation = jdbcTemplate.query("SELECT c.*,img.* FROM companies c LEFT JOIN images img ON c.id=img.company_id where (c.name =? OR c.location=? OR TRUE) ORDER BY c.timestamp DESC", new Object[]{name, location}, new RowMapper<Company>() {
            @Override
            public Company mapRow(ResultSet resultSet, int i) throws SQLException {
                Company company = new Company();
                company.setId(resultSet.getString("c.id"));
                company.setName(resultSet.getString("name"));
                company.setLocation(resultSet.getString("location"));
                company.setLatitude(resultSet.getDouble("latitude"));
                company.setLongitude(resultSet.getDouble("longitude"));
                company.setTimestamp(resultSet.getDate("timestamp"));

                Image image = new Image();
                image.setId(resultSet.getString("img.id"));
                image.setCompanyId(resultSet.getString("company_id"));
                image.setIcon(resultSet.getBoolean("is_icon"));
                image.setUrl(resultSet.getString("url"));

                company.setIconImage(image);

                return company;
            }
        });

        return allByNameOrLocation;
    }

    @Transactional
    public Company addCompany(Company company) {
        Company save = companyRepository.save(company);
        return save;
    }

    @Transactional
    public Company updateCompany(Company company) {
        Company save = companyRepository.save(company);
        return save;
    }

    @Transactional(readOnly = true)
    public Company getCompany(String companyId) {

        Company save = jdbcTemplate.queryForObject("SELECT c.*,img.* FROM companies c LEFT JOIN images img ON c.id=img.company_id where c.id =? ", new Object[]{companyId}, new RowMapper<Company>() {
            @Override
            public Company mapRow(ResultSet resultSet, int i) throws SQLException {
                Company company = new Company();
                company.setId(resultSet.getString("c.id"));
                company.setName(resultSet.getString("name"));
                company.setLocation(resultSet.getString("location"));
                company.setLatitude(resultSet.getDouble("latitude"));
                company.setLongitude(resultSet.getDouble("longitude"));

                Image image = new Image();
                image.setId(resultSet.getString("img.id"));
                image.setCompanyId(resultSet.getString("company_id"));
                image.setIcon(resultSet.getBoolean("is_icon"));
                image.setUrl(resultSet.getString("url"));

                company.setIconImage(image);

                return company;
            }
        });

        return save;
    }

    @Transactional
    public String deleteCompany(String companyId) {
        companyRepository.delete(companyId);
        imageRepository.deleteAllByProductIdOrCategoryIdOrCompanyId(companyId, companyId, companyId);
        return "done";
    }

    @Transactional
    public String addTopCompany(String id) {
        companyRepository.addTopCompany(id);

        return "done";
    }

    public List<Product> getCompanyForUser(String name) {

        User byUsername = userRepository.findByUsername(name);

        Map namedParameters = new HashMap<>();
        namedParameters.put("companyId", byUsername.getCompanyId());

        List<Product> productLists = namedParameterJdbcTemplate.query(PRODUCT_SQL.GET_ALL_PRODUCTS_FOR_COMPANY_SQL, namedParameters, (ResultSetExtractor<List<Product>>) resultSet -> {
            List<Product> products = new ArrayList<>();

            List<String> ids = new ArrayList<>();
            while (resultSet.next()) {
                if (resultSet.getString("id") != null && !ids.contains(resultSet.getString("id"))) {
                    Product product = new Product();
                    product.setId(resultSet.getString("p.id"));
                    product.setCategoryId(resultSet.getString("category_id"));
                    product.setCompanyId(resultSet.getString("company_id"));
                    product.setDateFrom(resultSet.getString("date_from"));
                    product.setDateTo(resultSet.getString("date_to"));
                    product.setDescription(resultSet.getString("description"));
                    product.setName(resultSet.getString("p.name"));
                    product.setOldPrice(resultSet.getDouble("old_price"));
                    product.setNewPrice(resultSet.getDouble("new_price"));

                    Category category = new Category();
                    category.setId(resultSet.getString("ca.id"));
                    category.setName(resultSet.getString("ca.name"));

                    Image categoryImage = new Image();
                    categoryImage.setId(resultSet.getString("ca_image.id"));
                    categoryImage.setCategoryId(resultSet.getString("ca_image.category_id"));
                    categoryImage.setUrl(resultSet.getString("ca_image.url"));
                    categoryImage.setIcon(resultSet.getBoolean("ca_image.is_icon"));
                    category.setIconImage(categoryImage);


                    Company company = new Company();
                    company.setId(resultSet.getString("co.id"));
                    company.setName(resultSet.getString("co.name"));
                    company.setLocation(resultSet.getString("location"));
                    company.setLatitude(resultSet.getDouble("latitude"));
                    company.setLongitude(resultSet.getDouble("longitude"));

                    Image companyImage = new Image();
                    companyImage.setId(resultSet.getString("co_image.id"));
                    companyImage.setCompanyId(resultSet.getString("co_image.company_id"));
                    companyImage.setUrl(resultSet.getString("co_image.url"));
                    companyImage.setIcon(resultSet.getBoolean("co_image.is_icon"));
                    company.setIconImage(companyImage);

                    product.setCompany(company);
                    product.setCategory(category);
                    products.add(product);
                }
                ids.add(resultSet.getString("id"));
            }
            resultSet.beforeFirst();

            List<Image> images = new ArrayList<>();
            while (resultSet.next()) {
                if (resultSet.getString("img.id") != null) {
                    Image image = new Image();
                    image.setId(resultSet.getString("img.id"));
                    image.setCategoryId(resultSet.getString("img.category_id"));
                    image.setCompanyId(resultSet.getString("img.company_id"));
                    image.setProductId(resultSet.getString("product_id"));
                    image.setUrl(resultSet.getString("url"));
                    image.setIcon(resultSet.getBoolean("is_icon"));

                    images.add(image);
                }
            }

            for (Product product : products) {
                List<Image> imageList = new ArrayList<>();
                for (Image image : images) {
                    if (image.getProductId().equals(product.getId())) {
                        if (image.isIcon()) {
                            product.setIconImage(image);
                        } else {
                            imageList.add(image);
                        }
                    }
                }
                product.setImageList(imageList);
            }


            return products;
        });

        return productLists;
    }
}
