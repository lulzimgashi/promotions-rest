package com.pertoke.api.services;

import com.pertoke.api.exceptions.ObjectAlreadyExistException;
import com.pertoke.api.exceptions.ObjectNotFoundException;
import com.pertoke.api.models.Authority;
import com.pertoke.api.models.User;
import com.pertoke.api.repositories.AuthorityRepository;
import com.pertoke.api.repositories.UserRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by lulzimgashi on 14/06/2017.
 */
@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthorityRepository authorityRepository;

    @Transactional
    public User addUser(User user, Authentication authentication) throws Throwable {
        validateLoggedUser(user, authentication.getAuthorities().toArray()[0]);


        user.setPassword(new StandardPasswordEncoder().encode(user.getPassword()));
        User save = userRepository.save(user);

        Authority authority = new Authority();
        authority.setUsername(save.getUsername());
        authority.setAuthority(user.getRole());
        authority.setUserId(save.getId());

        authorityRepository.save(authority);


        User newUser = new User();

        BeanUtils.copyProperties(save, newUser);

        newUser.setPassword("PROTECTED");
        return newUser;
    }

    private void validateLoggedUser(User user, Object o) throws ObjectAlreadyExistException {
        if (o.toString().equals("ROLE_ANONYMOUS") && !user.getRole().equals("ROLE_USER")) {
            throw new AccessDeniedException("You can't add user without role USER");
        }
        if (o.toString().equals("ROLE_COMPANY") && !user.getRole().equals("ROLE_USER")) {
            throw new AccessDeniedException("You can't add user without role USER");
        }
        if (o.toString().equals("ROLE_USER") && !user.getRole().equals("ROLE_USER")) {
            throw new AccessDeniedException("You can't add user without role USER");
        }
    }

    @Transactional
    public User updateUser(User user, Authentication authentication) throws Throwable {
        validateLoggedUser(user, authentication.getAuthorities().toArray()[0]);

        User loggedUser = userRepository.findByUsername(authentication.getName());

        if (!loggedUser.getId().equals(user.getId())) {
            throw new AccessDeniedException("You don't have access to edit that user");
        }

        boolean exist = userRepository.checkIfExist(user.getUsername(), loggedUser.getId());
        if (exist) {
            throw new ObjectAlreadyExistException("User with same username exists");
        }

        User one = userRepository.findOne(user.getId());
        if (one == null) {
            throw new ObjectNotFoundException("User doesn't exists");
        }

        user.setPassword(one.getPassword());
        User save = userRepository.save(user);
        save.setRole(user.getRole());

        authorityRepository.updateAuthority(save.getUsername(), user.getRole(), save.getId());

        User newUser = new User();

        BeanUtils.copyProperties(save, newUser);

        newUser.setPassword("PROTECTED");
        return newUser;

    }

    @Transactional
    public String deleteUser(String id, String username) {
        User loggedUser = userRepository.findByUsername(username);

        if (!loggedUser.getId().equals(id)) {
            throw new AccessDeniedException("You don't have access to edit that user");
        }

        userRepository.dUser(id);
        authorityRepository.deleteByUserId(id);
        return "Done";
    }

    @Transactional(readOnly = true)
    public User getUser(String userId) {
        User one = userRepository.findByIdAndEnabledTrue(userId);
        if (one != null) {
            one.setPassword("PROTECTED");
        }
        Authority authority = authorityRepository.findByUsername(one.getUsername());
        one.setRole(authority.getAuthority());
        return one;
    }

    @Transactional
    public String changePassword(User user, String username) throws Throwable {
        User loggedUser = userRepository.findByUsername(username);
        User dbUser = userRepository.findOne(user.getId());

        if (!loggedUser.getId().equals(dbUser.getId())) {
            throw new AccessDeniedException("You don't have access to edit that user");
        }

        dbUser.setPassword(new StandardPasswordEncoder().encode(user.getPassword()));

        userRepository.save(dbUser);

        return "done";
    }

    @Transactional(readOnly = true)
    public User getLoggedUser(String name) {

        User byUsername = userRepository.findByUsername(name);
        Authority authority = authorityRepository.findByUsername(byUsername.getUsername());

        byUsername.setPassword("PROTECTED");
        byUsername.setRole(authority.getAuthority());

        return byUsername;
    }
}