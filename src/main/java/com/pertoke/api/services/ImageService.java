package com.pertoke.api.services;

import com.pertoke.api.models.Image;
import com.pertoke.api.repositories.ImageRepository;
import com.pertoke.api.utilities.MyFTPclient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sun.misc.BASE64Decoder;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.UUID;

/**
 * Created by lulzimgashi on 13/06/2017.
 */
@Service
public class ImageService {

    @Autowired
    private ImageRepository imageRepository;

    @Autowired
    private MyFTPclient myFTPclient;

    @Value("${ftp-host}")
    private String ftp_host;

    @Transactional
    public Image upload(InputStream inputStream, String companyId, String categoryId, String productId, boolean isIcon) throws IOException {

        String uuid = UUID.randomUUID().toString();


        myFTPclient.upload(inputStream, uuid);

        Image image = new Image();
        image.setId(uuid);
        image.setIcon(isIcon);
        image.setProductId(productId);
        image.setCategoryId(categoryId);
        image.setCompanyId(companyId);

        image.setUrl(ftp_host + "/images/" + uuid + ".png");

        return imageRepository.save(image);
    }


    @Transactional
    public String deleteImage(String id) throws IOException {
        Image one = imageRepository.findOne(id);

        if (one != null) {
            imageRepository.delete(id);
            myFTPclient.delete(one.getUrl());
        }
        return "done";
    }

    public List<Image> getImagesByParent(String parentId) {
        List<Image> allByCategoryIdOrProductIdOrCompanyId = imageRepository.findAllByCategoryIdOrProductIdOrCompanyId(parentId, parentId, parentId);

        return allByCategoryIdOrProductIdOrCompanyId;
    }

    public Image getImage(String imageId) {
        Image one = imageRepository.findOne(imageId);

        return one;
    }
}