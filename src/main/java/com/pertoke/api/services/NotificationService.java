package com.pertoke.api.services;

import com.pertoke.api.models.Image;
import com.pertoke.api.models.Notification;
import com.pertoke.api.models.Product;
import com.pertoke.api.repositories.ImageRepository;
import com.pertoke.api.repositories.NotificationRepository;
import com.pertoke.api.repositories.ProductRepository;
import com.pertoke.api.utilities.FCMHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by lulzimgashi on 25/07/2017.
 */
@Service
public class NotificationService {

    @Autowired
    private FCMHelper fcmHelper;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ImageRepository imageRepository;

    @Autowired
    private NotificationRepository notificationRepository;

    public String sendNotification(String id) {
        Product one = productRepository.findOne(id);
        if (one != null) {
            try {
                String s = fcmHelper.sendNotification(one.getName(), one.getDescription(), one.getId());
                if (s != null) {
                    Image image = imageRepository.imageByProductId(id);
                    Notification notification = new Notification();
                    notification.setPostId(id);
                    notification.setTitle(one.getName());
                    notification.setBody(one.getDescription());

                    if (image != null) {
                        notification.setThumbnail(image.getUrl());
                    }

                    notificationRepository.save(notification);

                    return "Success";
                } else {
                    return null;
                }
            } catch (Exception e) {
                return null;
            }
        }
        return null;
    }

    public List<Notification> getAll() {
        List<Notification> all = notificationRepository.findAll();
        return all;
    }
}
