package com.pertoke.api.services;

import com.pertoke.api.models.Category;
import com.pertoke.api.models.Image;
import com.pertoke.api.repositories.CategoryRepository;
import com.pertoke.api.repositories.ImageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by lulzimgashi on 13/06/2017.
 */
@Service
public class CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private ImageRepository imageRepository;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Transactional(readOnly = true)
    public List<Category> getAllCategories() {

        List<Category> query = jdbcTemplate.query("select c.*,img.* from categories c LEFT JOIN images img ON c.id=img.category_id", new RowMapper<Category>() {
            @Override
            public Category mapRow(ResultSet resultSet, int i) throws SQLException {
                Category category = new Category();
                category.setId(resultSet.getString("c.id"));
                category.setName(resultSet.getString("name"));

                Image image = new Image();
                image.setId(resultSet.getString("img.id"));
                image.setUrl(resultSet.getString("url"));
                image.setIcon(resultSet.getBoolean("is_icon"));
                image.setCategoryId(resultSet.getString("category_id"));

                category.setIconImage(image);
                return category;
            }
        });

        return query;
    }

    @Transactional
    public Category addCategory(Category category) {
        Category save = categoryRepository.save(category);

        return save;
    }

    @Transactional
    public Category updateCategory(Category category) {
        Category save = categoryRepository.save(category);

        return save;
    }

    @Transactional(readOnly = true)
    public Category getCategory(String categoryId) {
        Category category = jdbcTemplate.queryForObject("select c.*,img.* from categories c LEFT JOIN images img ON c.id=img.category_id where c.id=?", new Object[]{categoryId}, new RowMapper<Category>() {
            @Override
            public Category mapRow(ResultSet resultSet, int i) throws SQLException {
                Category category = new Category();
                category.setId(resultSet.getString("c.id"));
                category.setName(resultSet.getString("name"));

                Image image = new Image();
                image.setId(resultSet.getString("img.id"));
                image.setUrl(resultSet.getString("url"));
                image.setIcon(resultSet.getBoolean("is_icon"));
                image.setCategoryId(resultSet.getString("category_id"));

                category.setIconImage(image);
                return category;
            }
        });

        return category;
    }

    @Transactional
    public String deleteCategory(String categoryId) {
        categoryRepository.delete(categoryId);
        imageRepository.deleteAllByProductIdOrCategoryIdOrCompanyId(categoryId, categoryId, categoryId);
        return "done";
    }
}
