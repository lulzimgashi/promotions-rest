package com.pertoke.api.controllers;

import com.pertoke.api.exceptions.ObjectNotFoundException;
import com.pertoke.api.models.User;
import com.pertoke.api.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

/**
 * Created by lulzimgashi on 14/06/2017.
 */
@RestController
@RequestMapping("users")
public class UserController {

    @Autowired
    private UserService userService;


    @RequestMapping(value = "/new", method = RequestMethod.POST, consumes = {"application/json"})
    public ResponseEntity<User> addUser(@RequestBody User user) throws Throwable {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        User savedUser = userService.addUser(user, authentication);
        return new ResponseEntity<>(savedUser, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('USER','COMPANY')")
    @RequestMapping(value = "/update", method = RequestMethod.PUT, consumes = {"application/json"})
    public ResponseEntity<User> updateUser(@RequestBody User user) throws Throwable {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        User savedUser = userService.updateUser(user, authentication);
        return new ResponseEntity<>(savedUser, HttpStatus.OK);
    }

    @RequestMapping(value = "/{userId}", method = RequestMethod.GET, produces = {"application/json"})
    public ResponseEntity<User> getUser(@PathVariable String userId) {
        User user = userService.getUser(userId);

        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('USER','COMPANY')")
    @RequestMapping(value = "/delete/{userId}", method = RequestMethod.DELETE)
    public ResponseEntity<String> updateUser(@PathVariable String userId) throws ObjectNotFoundException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        String message = userService.deleteUser(userId, authentication.getName());

        return new ResponseEntity<>(message, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('USER','COMPANY')")
    @RequestMapping(value = "/password", method = RequestMethod.PUT)
    public ResponseEntity<String> changePassword(@RequestBody User user, @RequestParam("type") String type) throws Throwable {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();


        String message = userService.changePassword(user, authentication.getName());

        return new ResponseEntity<>(message, HttpStatus.OK);
    }


    @RequestMapping(value = "/login", method = RequestMethod.GET, produces = {"application/json"})
    public ResponseEntity<User> getLoginUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        User user = userService.getLoggedUser(authentication.getName());
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

}