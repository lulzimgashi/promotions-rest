package com.pertoke.api.controllers;

import com.pertoke.api.models.Notification;
import com.pertoke.api.services.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by lulzimgashi on 25/07/2017.
 */
@RestController
@RequestMapping("/notify")
public class NotificationController {

    @Autowired
    private NotificationService notificationService;

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public ResponseEntity<String> addCompany(@PathVariable String id) {
        String result = notificationService.sendNotification(id);
        if (result == null) {
            result = "Fail";
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<List<Notification>> getAll() {
        List<Notification> all = notificationService.getAll();

        return new ResponseEntity<>(all, HttpStatus.OK);
    }
}
