package com.pertoke.api.controllers;

import com.pertoke.api.models.Company;
import com.pertoke.api.models.Product;
import com.pertoke.api.services.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by lulzimgashi on 13/06/2017.
 */
@RestController
@RequestMapping("companies")
public class CompanyController {

    @Autowired
    private CompanyService companyService;


    @RequestMapping(value = "", method = RequestMethod.GET, produces = {"application/json"})
    public ResponseEntity<List<Company>> getAllCompanies(@RequestParam(name = "name", required = false) String name,
                                                         @RequestParam(name = "location", required = false) String location) {

        List<Company> allCompaniesByFilters = companyService.getAllCompaniesByFilters(name, location);

        return new ResponseEntity<>(allCompaniesByFilters, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "/new", method = RequestMethod.POST, consumes = {"application/json"}, produces = {"application/json"})
    public ResponseEntity<Company> addCompany(@RequestBody Company company) {
        Company company1 = companyService.addCompany(company);

        return new ResponseEntity<>(company1, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "/top_company/{id}", method = RequestMethod.PUT, consumes = {"application/json"}, produces = {"application/json"})
    public ResponseEntity<String> addTopCompany(@PathVariable String id) {
        String company1 = companyService.addTopCompany(id);

        return new ResponseEntity<>(company1, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('COMPANY')")
    @RequestMapping(value = "/my_company", method = RequestMethod.GET, produces = {"application/json"})
    public ResponseEntity<List<Product>> myCompany() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        List<Product> products = companyService.getCompanyForUser(authentication.getName());

        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "/update", method = RequestMethod.PUT, consumes = {"application/json"}, produces = {"application/json"})
    public ResponseEntity<Company> updateCompany(@RequestBody Company company) {
        Company company1 = companyService.updateCompany(company);

        return new ResponseEntity<>(company1, HttpStatus.OK);
    }

    @RequestMapping(value = "/{companyId}", method = RequestMethod.GET, produces = {"application/json"})
    public ResponseEntity<Company> getCompany(@PathVariable String companyId) {
        Company company = companyService.getCompany(companyId);

        return new ResponseEntity<>(company, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "/delete/{companyId}", method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteCompany(@PathVariable String companyId) {
        String s = companyService.deleteCompany(companyId);

        return new ResponseEntity<>(s, HttpStatus.OK);
    }

}
