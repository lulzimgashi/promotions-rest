package com.pertoke.api.controllers;

import com.pertoke.api.models.Product;
import com.pertoke.api.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by lulzimgashi on 13/06/2017.
 */
@RestController
@RequestMapping("products")
public class ProductController {

    @Autowired
    private ProductService productService;

    @RequestMapping(value = "", method = RequestMethod.GET, produces = {"application/json"})
    public ResponseEntity<List<Product>> getAllProducts(@RequestParam(value = "companyName", required = false) String companyName,
                                                        @RequestParam(value = "categoryName", required = false) String categoryName,
                                                        @RequestParam(value = "dateFrom", required = false) String dateFrom,
                                                        @RequestParam(value = "dateTo", required = false) String dateTo,
                                                        @RequestParam(value = "name", required = false) String name,
                                                        @RequestParam(value = "price", required = false) Double price,
                                                        @RequestParam(value = "start", required = true) int start,
                                                        @RequestParam(value = "end", required = true) int end) {
        List<Product> allByFilters = productService.allByFilters(companyName, categoryName, dateFrom, dateTo, name, price, start, end);

        return new ResponseEntity<>(allByFilters, HttpStatus.OK);

    }

    @PreAuthorize("hasRole('COMPANY')")
    @RequestMapping(value = "/new", method = RequestMethod.POST, consumes = {"application/json"}, produces = {"application/json"})
    public ResponseEntity<Product> addProduct(@RequestBody Product product) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Product product1 = productService.addProduct(product, authentication.getName());

        return new ResponseEntity<>(product1, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('COMPANY')")
    @RequestMapping(value = "", method = RequestMethod.PUT, consumes = {"application/json"}, produces = {"application/json"})
    public ResponseEntity<Product> updateProduct(@RequestBody Product product) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Product product1 = productService.addProduct(product, authentication.getName());

        return new ResponseEntity<>(product1, HttpStatus.OK);
    }

    @RequestMapping(value = "/{productId}", method = RequestMethod.GET, produces = {"application/json"})
    public ResponseEntity<Product> getProduct(@PathVariable String productId) {
        Product product = productService.getProduct(productId);

        return new ResponseEntity<>(product, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('COMPANY')")
    @RequestMapping(value = "/delete/{productId}", method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteProduct(@PathVariable String productId) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String s = productService.deleteProduct(productId, authentication.getName());

        return new ResponseEntity<>(s, HttpStatus.OK);
    }
}
