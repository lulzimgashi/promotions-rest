package com.pertoke.api.controllers;

import com.pertoke.api.models.Image;
import com.pertoke.api.models.ImgObj;
import com.pertoke.api.models.User;
import com.pertoke.api.services.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Created by lulzimgashi on 13/06/2017.
 */
@RestController
@RequestMapping("images")
public class ImageController {

    @Autowired
    private ImageService imageService;

    @RequestMapping(value = "/{parentId}", method = RequestMethod.GET, produces = {"application/json"})
    public ResponseEntity<Object> getAllImages(@PathVariable String parentId,
                                               @RequestParam("type") String type) {
        if (type.equals("one")) {
            Image image = imageService.getImage(parentId);
            return new ResponseEntity<>(image, HttpStatus.OK);
        } else {
            List<Image> images = imageService.getImagesByParent(parentId);
            return new ResponseEntity<>(images, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<Image> addImage(@RequestParam("file") MultipartFile file,
                                          @RequestParam(required = false) String companyId,
                                          @RequestParam(required = false) String categoryId,
                                          @RequestParam(required = false) String productId,
                                          Boolean isIcon) throws IOException {

        Image upload = imageService.upload(file.getInputStream(), companyId, categoryId, productId, isIcon);

        return new ResponseEntity<>(upload, HttpStatus.OK);
    }


    @RequestMapping(value = "/{imageId}", method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteImage(@PathVariable String imageId) throws IOException {

        String s = imageService.deleteImage(imageId);

        return new ResponseEntity<>(s, HttpStatus.OK);
    }
}
